import { Directive, ElementRef, Input, Renderer2 } from '@angular/core';
import { RouletteService } from '../shared/roulette.service';


@Directive({
  selector: '[appColorClass]',
})

export class ColorDirective {
  @Input() set appColorClass(number: string) {
    this.renderer.addClass(this.el.nativeElement, this.rouletteService.getColor(parseInt(number)));
  }

  constructor(
    private el: ElementRef,
    private renderer: Renderer2,
    private rouletteService: RouletteService
  ) {}
}
