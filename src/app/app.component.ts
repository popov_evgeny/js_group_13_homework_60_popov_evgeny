import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { RouletteService } from './shared/roulette.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {

  @Input() colorBid = '';
  @ViewChild('inputBid') inputBid!: ElementRef;

  newNumber!: number;
  arrayNumbers: number[] = [];
  balance = 100;
  disabled = true;

  constructor(private rouletteService: RouletteService) {}

  ngOnInit(): void {
    this.newNumber = this.rouletteService.generateNumber();
    this.rouletteService.newNumber.subscribe((number: number) => {
      this.newNumber = number;
      this.generateNumbers();
      this.onCheckTheAccount();
    });
  }

  generateNumbers() {
    this.arrayNumbers.push(this.newNumber);
  }

  onclickStart() {
    this.disabled = false;
    this.rouletteService.start();
  }

  onClickStop() {
    this.rouletteService.stop();
    this.inputBid.nativeElement.value = 1;
    this.disabled = true;
  }

  onclickReset() {
    this.arrayNumbers.splice(0, this.arrayNumbers.length);
  }

  onCheckTheAccount() {
    const color = this.rouletteService.getColor(this.newNumber);

    if (this.colorBid === '') {
      alert('Please select a bet color!');
      this.onClickStop();
      this.onclickReset();
    } else {
      if (this.colorBid === 'zero'){
        if (this.colorBid === color) {
          this.balance = this.balance + (parseInt(this.inputBid.nativeElement.value) * 35);
        } else {
          this.balance = this.balance - parseInt(this.inputBid.nativeElement.value);
        }
      } else {
        if (this.colorBid === color){
          this.balance = this.balance + parseInt(this.inputBid.nativeElement.value);
        } else {
          this.balance = this.balance - parseInt(this.inputBid.nativeElement.value);
        }
      }
    }
  }
}
