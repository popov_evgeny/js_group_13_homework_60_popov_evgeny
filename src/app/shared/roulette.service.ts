import { EventEmitter } from '@angular/core';

export class RouletteService {
  newNumber = new EventEmitter<number>();
  interval = 0;

  generateNumber() {
    return Math.floor(Math.random() * 37);
  }

  start() {
    if (this.interval) return;
    this.interval = setInterval(() => {
      this.newNumber.emit(this.generateNumber());
    },1000);
  }

  stop() {
    clearInterval(this.interval);
    this.interval = 0;
  }

  getColor(number: number) {
    let color = '';
    if (number === 0) {
      color = 'zero';
    } else if ((number >= 1 && number <= 10) || (number >= 19 && number <= 28)) {
      if (number %2 !== 0) {
        color = 'red';
      } else {
        color = 'black';
      }
    } else if ((number >= 11 && number <= 18) || (number >= 28 && number <= 37)) {
      if (number %2 !== 0) {
        color = 'black';
      } else {
        color = 'red';
      }
    }
    return color;
  }
}
